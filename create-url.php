<?php

if (get_headers($_POST['url'], 1)) {
	$longurl =  $_POST['url'];
   	$customurl =  preg_replace('/[^0-9a-zA-Z_-]+/', '', $_POST['customurl']);
	$symbols = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$symbols = str_split($symbols);

	if (empty($customurl)) {	
		while (true) { 
			shuffle($symbols);
			$shorturl = implode('', array_slice($symbols, 0, 5));
			$file = 'links/' . $shorturl;
			if (!file_exists($file)) {
				break;
			}
		}
	} else {
		if (file_exists('links/' . $customurl)) {		
			echo "Выбранное значение окончания ссылки уже занято, выберите другое или оставьте поле пустым";
			exit;
		} else {
			$shorturl = $customurl;
			$file = 'links/' . $customurl;
		}	
	}

	$file = fopen($file, "w"); 
    fwrite($file, $longurl);
    fclose($file);
    echo $shorturl;    
} else {
  	echo "Пустое поле ссылки или ссылка введена некоректно";
}
