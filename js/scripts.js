$('button').click(function() { 
    $('a').html('');   
    $.ajax({
        type: 'POST',
        url: 'create-url.php',
        data: 'url=' + $("#longurl").val() + '&customurl=' + $("#customurl").val(),
        success: function(result) { 
            if (/[а-я]+/.test(result)) {
                alert(result);
            } else {
                $('input').val("");
                $('a').html(window.location.href + result); 
                $('a').attr('href', result);
            }
        }
    });
});