<?php

$file = 'links/' . $path;

if (file_exists($file)) {
    $file = fopen($file, "r"); 
    header('Location: ' . fgets($file));	
	fclose($file);
} else {
	echo "Ошибка! URL не найден.";
}
